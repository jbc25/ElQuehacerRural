package net.elquehacerrural.app.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import net.elquehacerrural.app.model.Palabra;

import java.util.List;

@Dao
public interface PalabraDao {


    @Query("SELECT * FROM palabra ORDER BY nombre")
    List<Palabra> getAll();

    @Query("SELECT * FROM palabra WHERE nombre LIKE :text || '%' ORDER BY nombre")
    List<Palabra> search(String text);

    @Query("SELECT * FROM palabra WHERE nombre LIKE :text || '%' AND quintilla NOT NULL ORDER BY nombre")
    List<Palabra> searchWithQuintilla(String text);

    @Query("SELECT * FROM palabra WHERE quintilla NOT NULL")
    List<Palabra> getAllHasQuintilla();

    @Query("SELECT * FROM palabra WHERE id = :id")
    Palabra getById(int id);

//    @Query("SELECT * FROM user WHERE first_name LIKE :first AND "
//            + "last_name LIKE :last LIMIT 1")
//    User findByName(String first, String last);

    @Insert
    void insertAll(List<Palabra> palabras);

    @Insert
    void insert(Palabra palabras);

    @Update
    void update(Palabra palabra);

    @Delete
    void delete(Palabra palabra);

}
