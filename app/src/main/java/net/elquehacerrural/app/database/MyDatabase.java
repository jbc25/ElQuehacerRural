package net.elquehacerrural.app.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import net.elquehacerrural.app.model.Palabra;

@Database(entities = {Palabra.class}, version = 2, exportSchema = false)
public abstract class MyDatabase extends RoomDatabase {
    public abstract PalabraDao palabraDao();
}
