package net.elquehacerrural.app;

import android.app.Application;
import androidx.room.Room;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import net.elquehacerrural.app.database.MyDatabase;

/**
 * Created by julio on 17/06/16.
 */

public class App extends Application {

    private static final String TAG = "App";

    public static final String PREFIX = "net.elquehacerrural.app.";
    public static final String SHARED_DATA_STORED = PREFIX + "shared_data_stored";
    private static final String SHARED_RELOAD_DB_LETTER_M = PREFIX + ".shared_reload_db_letter_m";
    private static MyDatabase db;
    //    public static final String SHARED_TOKEN = PREFIX + "shared_token";

    public static final String DB_NAME = "database-main";

    @Override
    public void onCreate() {
        super.onCreate();

        // Set up Crashlytics, disabled for debug builds
//        Crashlytics crashlyticsKit = new Crashlytics.Builder()
//                .core(new CrashlyticsCore.Builder()
//                        .disabled(DebugHelper.SWITCH_DISABLE_CRASHLYTICS)
//                        .build())
//                .build();
//
//        // Initialize Fabric with the debug-disabled crashlytics.
//        Fabric.with(this, crashlyticsKit);

        if (getPrefs(getApplicationContext()).getBoolean(SHARED_RELOAD_DB_LETTER_M, true)) {
            getPrefs(getApplicationContext()).edit().putBoolean(App.SHARED_DATA_STORED, false).commit();
            getPrefs(getApplicationContext()).edit().putBoolean(App.SHARED_RELOAD_DB_LETTER_M, false).commit();
        }


        db = Room.databaseBuilder(getApplicationContext(),
                MyDatabase.class, DB_NAME)
//                .addMigrations()
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build();


    }


    public static MyDatabase getDB() {
        return db;
    }

    public static SharedPreferences getPrefs(Context context) {
//        return new SecurePreferences(context);
        return PreferenceManager.getDefaultSharedPreferences(context);
    }


}
