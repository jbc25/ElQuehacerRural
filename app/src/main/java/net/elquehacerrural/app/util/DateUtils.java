package net.elquehacerrural.app.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by julio on 27/02/16.
 */
public class DateUtils {

    public static SimpleDateFormat formatDateTimeApi = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static SimpleDateFormat formatDateTimeUser = new SimpleDateFormat("dd/MM/yyyy '-' HH:mm");

    public static SimpleDateFormat formatDateApi = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat formatDateUser = new SimpleDateFormat("dd/MM/yyyy");

    public static SimpleDateFormat formatTimeUser = new SimpleDateFormat("HH:mm");

    public static SimpleDateFormat formatDateTimeApiSlashes = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    static {
        formatDateTimeApiSlashes.setTimeZone(TimeZone.getTimeZone("UTC"));
        formatDateTimeApi.setTimeZone(TimeZone.getTimeZone("UTC"));
        formatDateApi.setTimeZone(TimeZone.getTimeZone("UTC"));

//        formatDateTimeUser.setTimeZone(TimeZone.getTimeZone("UTC"));
//        formatDateUser.setTimeZone(TimeZone.getTimeZone("UTC"));
//        formatTimeUser.setTimeZone(TimeZone.getTimeZone("UTC"));
    }


    public static String getCurrentDateTimeHumanFormat() {
        return formatDateTimeUser.format(new Date());
    }

    public static String getCurrentDateApiFormat() {
        return formatDateApi.format(new Date());
    }

    public static String convertDateTimeApiToUserFormat(String dateTimeString) {

        try {
            Date date = DateUtils.formatDateTimeApiSlashes.parse(dateTimeString);
            String dateStr = DateUtils.formatDateTimeUser.format(date);
            return dateStr;
        } catch (ParseException | NullPointerException e) {
            try {
                Date date = DateUtils.formatDateTimeApi.parse(dateTimeString);
                String dateStr = DateUtils.formatDateTimeUser.format(date);
                return dateStr;
            } catch (ParseException | NullPointerException e1) {
                e1.printStackTrace();
            }
        }
        return dateTimeString;
    }

    public static String convertDateApiToUserFormat(String dateString) {

        try {
            Date date = formatDateApi.parse(dateString);
            return formatDateUser.format(date);
        } catch (Exception e) {
        }

        return dateString;
    }

    public static String convertTimeMillisToUserFormat(Long millis) {

        return formatDateTimeUser.format(new Date(millis));

    }

    public static String convertDateApiToUserFormat(Date date) {
        return formatDateUser.format(date);
    }
}
