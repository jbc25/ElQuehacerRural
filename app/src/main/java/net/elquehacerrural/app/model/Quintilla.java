package net.elquehacerrural.app.model;

import com.google.gson.annotations.SerializedName;

public class Quintilla {

    @SerializedName("quintilla_id")
    private int id;

    @SerializedName("quintilla_palabra_id")
    private int palabraId;

    @SerializedName("quintilla_texto")
    private String texto;

    // -----------


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPalabraId() {
        return palabraId;
    }

    public void setPalabraId(int palabraId) {
        this.palabraId = palabraId;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getTextoLineaCorregida() {

        String[] versos = getTexto().split("\n");

        if (versos.length > 5 && !versos[5].trim().isEmpty()) {
            int estrofas = versos.length / 5;
            for (int i = 1; i < estrofas; i++) {
                int posicUltimoVerso = i * 5;
                versos[posicUltimoVerso] = "\n" + versos[posicUltimoVerso].trim();
            }

            String textoCorregido = "";
            for (int i = 0; i < versos.length; i++) {
                String verso = versos[i];
                textoCorregido += verso + "\n";
            }

            return textoCorregido;
        }

        return getTexto();
    }
}
