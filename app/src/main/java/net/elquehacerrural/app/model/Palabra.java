package net.elquehacerrural.app.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity
public class Palabra implements Comparable<Palabra> {

    @PrimaryKey
    @SerializedName("palabra_id")
    private int id;

    @SerializedName("palabra_nombre")
    private String nombre;

    @SerializedName("palabra_definicion")
    private String definicion;

    private String quintilla;

    // -----------

    public boolean hasQuintilla() {
        return quintilla != null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDefinicion() {
        return definicion;
    }

    public void setDefinicion(String definicion) {
        this.definicion = definicion;
    }

    @Override
    public int compareTo(@NonNull Palabra palabra) {
        return getNombre().compareTo(palabra.getNombre());
    }

    public String getQuintilla() {
        return quintilla;
    }

    public void setQuintilla(String quintilla) {
        this.quintilla = quintilla;
    }
}
