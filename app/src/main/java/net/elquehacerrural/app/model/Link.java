package net.elquehacerrural.app.model;

public class Link {

    private String title;
    private int imgResId;
    private String link;

    public Link(String title, int imgResId, String link) {
        this.title = title;
        this.imgResId = imgResId;
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImgResId() {
        return imgResId;
    }

    public void setImgResId(int imgResId) {
        this.imgResId = imgResId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
