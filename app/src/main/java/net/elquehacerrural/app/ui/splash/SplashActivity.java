package net.elquehacerrural.app.ui.splash;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;

import net.elquehacerrural.app.App;
import net.elquehacerrural.app.R;
import net.elquehacerrural.app.base.BaseActivity;
import net.elquehacerrural.app.database.PalabraDao;
import net.elquehacerrural.app.model.Palabra;
import net.elquehacerrural.app.model.PalabrasJson;
import net.elquehacerrural.app.model.Quintilla;
import net.elquehacerrural.app.model.QuintillasJson;
import net.elquehacerrural.app.util.Util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import needle.Needle;
import needle.UiRelatedProgressTask;

import static net.elquehacerrural.app.App.DB_NAME;

public class SplashActivity extends BaseActivity {

    private List<Palabra> palabras;
    private List<Quintilla> quintillas;
    private View btnEnter;
    private View progressLoad;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        progressLoad = findViewById(R.id.progress_load);
        btnEnter = findViewById(R.id.btn_enter);
        btnEnter.setVisibility(View.GONE);

        btnEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if (!getPrefs().getBoolean(App.SHARED_DATA_STORED, false)) {
            // https://zsoltsafrany.github.io/needle/
            Needle.onBackgroundThread().execute(new UiRelatedProgressTask<Boolean, Integer>() {
                @Override
                protected Boolean doWork() {
                    boolean ok = decodeData();
                    if (!ok) {
                        return false;
                    }
                    deleteAllDB();
                    storeDB();
                    getPrefs().edit().putBoolean(App.SHARED_DATA_STORED, true).commit();
                    return true;

                }

                @Override
                protected void thenDoUiRelatedWork(Boolean result) {
                    if (result) {
                        enableEnterButton();
                    } else {
                        alert(getString(R.string.error_not_enought_memory));
                    }
                }

                @Override
                protected void onProgressUpdate(Integer progress) {

                }
            });
        } else {
            enableEnterButton();
        }


//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                finish();
//            }
//        }, Math.max(0, 2000 - diffMillis));

    }



    private void copyDatabase() throws IOException {
        File dbFile = new File("/data/data/net.elquehacerrural.app/databases", DB_NAME);
        InputStream is = getAssets().open(DB_NAME);
        OutputStream os = new FileOutputStream(dbFile);

        byte[] buffer = new byte[1024];
        while (is.read(buffer) > 0) {
            os.write(buffer);
        }

        os.flush();
        os.close();
        is.close();
    }

    private void enableEnterButton() {
        progressLoad.setVisibility(View.GONE);
        btnEnter.setVisibility(View.VISIBLE);
    }


    private void storeDB() {
        PalabraDao palabraDao = App.getDB().palabraDao();

        App.getDB().beginTransaction();

        Log.i(TAG, "--> start inserting palabras db");
        palabraDao.insertAll(palabras);

        Log.i(TAG, "--> start adding quintillas");
        for (Quintilla quintilla : quintillas) {
            Palabra palabra = palabraDao.getById(quintilla.getPalabraId());
            if (palabra != null) {
                palabra.setQuintilla(quintilla.getTextoLineaCorregida());
                palabraDao.update(palabra);
            }
        }

        App.getDB().setTransactionSuccessful();
        App.getDB().endTransaction();

        Log.i(TAG, "--> end process db");
    }

    private boolean decodeData() {

        long iniMillis = System.currentTimeMillis();

        Log.i(TAG, "--> start reading palabras");
        String jsonPalabras = Util.getStringFromAssets(this, "palabras.json");
        if (jsonPalabras != null) {

            Log.i(TAG, "--> start decoding palabras");
            PalabrasJson palabrasJson = new Gson().fromJson(jsonPalabras, PalabrasJson.class);
            List<Palabra> palabras = palabrasJson.palabras;
            List<Palabra> palabrasProcess = new ArrayList<>();
            for (int i = 0; i < palabras.size(); i++) {
                Palabra palabra = palabras.get(i);
                String nombre = palabra.getNombre();

                if (nombre == null || nombre.isEmpty()) {
                    continue;
                }

                if (nombre.startsWith("-")) {
                    nombre = nombre.replace("-", "");
                }
                if (nombre.startsWith("=1=")) {
                    nombre = nombre.replace("=1=", "");
                }
                nombre = nombre.trim();
                palabra.setNombre(nombre);

                palabrasProcess.add(palabra);

            }
            this.palabras = palabrasProcess;
        } else {
            return false;
        }

        Log.i(TAG, "--> start reading quintillas");
        String jsonQuintillas = Util.getStringFromAssets(this, "quintillas.json");
        if (jsonQuintillas != null) {

            Log.i(TAG, "--> start decoding quintillas");

            QuintillasJson quintillasJson = new Gson().fromJson(jsonQuintillas, QuintillasJson.class);
            List<Quintilla> quintillas = quintillasJson.quintillas;
            this.quintillas = quintillas;
        }

        Log.i(TAG, "--> end");

        long diffMillis = System.currentTimeMillis() - iniMillis;

        return true;

    }


    private void deleteAllDB() {

        App.getDB().clearAllTables();
    }
}
