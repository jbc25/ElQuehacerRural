package net.elquehacerrural.app.ui.links;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import net.elquehacerrural.app.R;
import net.elquehacerrural.app.model.Link;

import java.util.List;

public class LinksAdapter extends RecyclerView.Adapter<LinksAdapter.ViewHolder> {


    private List<Link> links;
    private Context context;
    private OnItemClickListener itemClickListener;


    public LinksAdapter(Context context, List<Link> links) {
        this.context = context;
        this.links = links;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View contactView = LayoutInflater.from(context).inflate(R.layout.row_link, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position2) {

        Link link = getItemAtPosition(holder.getAdapterPosition());

        holder.imgLink.setImageResource(link.getImgResId());
        holder.tvLinkTitle.setText(link.getTitle());
        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Link link = getItemAtPosition(holder.getAdapterPosition());
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link.getLink())));

            }
        });


    }

    @Override
    public int getItemCount() {
        return links.size();
    }

    public Link getItemAtPosition(int position) {
        return links.get(position);
    }

    public void updateData(List<Link> links) {
        this.links = links;
        notifyDataSetChanged();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView imgLink;
        private final TextView tvLinkTitle;
        public View rootView;

        public ViewHolder(View itemView) {

            super(itemView);

            imgLink = (ImageView) itemView.findViewById(R.id.img_link);
            tvLinkTitle = (TextView) itemView.findViewById(R.id.tv_link_title);

            rootView = itemView;
        }

    }


    public void setOnItemClickListener(OnItemClickListener listener) {
        this.itemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}


