package net.elquehacerrural.app.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import net.elquehacerrural.app.R;
import net.elquehacerrural.app.base.BaseActivity;
import net.elquehacerrural.app.util.Util;

public class InfoActivity extends BaseActivity {

    public static final int INFO_BIOGRAPHY = 0;
    public static final int INFO_PRESENTATION = 1;
    public static final int INFO_EDITION = 2;
    private ImageView imgManuel;

    public static void launchInfoActivity(Context context, int type) {
        Intent intent = new Intent(context, InfoActivity.class);
        intent.putExtra(Util.EXTRA_INT, type);
        context.startActivity(intent);
    }

    private TextView tvInfoTitle;
    private TextView tvInfoText;
    private TextView tvInfoFoot;

    private void findViews() {
        tvInfoTitle = (TextView)findViewById( R.id.tv_info_title );
        tvInfoText = (TextView)findViewById( R.id.tv_info_text );
        tvInfoFoot = (TextView)findViewById( R.id.tv_info_foot );
        imgManuel = findViewById(R.id.img_manuel);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        findViews();
        configureSecondLevelActivity();

        String infoTitle = null;
        String infoText = null;
        String infoFoot = null;

        int typeInfoScreen = getIntent().getIntExtra(Util.EXTRA_INT, 0);
        switch (typeInfoScreen) {
            case INFO_BIOGRAPHY:
                infoTitle = getString(R.string.short_biography);
                infoText = getString(R.string.biography_text);
                infoFoot = getString(R.string.biography_redactors);
                imgManuel.setImageResource(R.mipmap.img_manuel);
                break;

            case INFO_PRESENTATION:
                infoTitle = getString(R.string.presentation_title);
                infoText = getString(R.string.presentation_text);
                infoFoot = getString(R.string.presentation_foot);
                imgManuel.setImageResource(R.mipmap.img_manuel_90);
                break;

            case INFO_EDITION:
                infoTitle = getString(R.string.edition_title);
                infoText = getString(R.string.edition_text);
                break;
        }

        tvInfoTitle.setText(infoTitle);
        Util.setHtmlLinkableText(tvInfoText, infoText);
        tvInfoFoot.setText(infoFoot);


    }
}
