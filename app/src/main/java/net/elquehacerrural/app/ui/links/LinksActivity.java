package net.elquehacerrural.app.ui.links;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import net.elquehacerrural.app.R;
import net.elquehacerrural.app.base.BaseActivity;
import net.elquehacerrural.app.model.Link;

import java.util.ArrayList;
import java.util.List;

public class LinksActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_links);

        configureSecondLevelActivity();

        RecyclerView recyclerLinks = findViewById(R.id.recycler_links);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerLinks.setLayoutManager(layoutManager);

        List<Link> links = generateLinks();
        LinksAdapter adapter = new LinksAdapter(this, links);
        recyclerLinks.setAdapter(adapter);

    }

    private List<Link> generateLinks() {
        List<Link> links = new ArrayList<>();

        links.add(new Link("El diccionario en la Biblioteca Nacional de España", R.mipmap.img_link_bne, "http://datos.bne.es/edicion/a5063646.html"));
        links.add(new Link("El diccionario en la Biblioteca de la Universidad de Murcia", R.mipmap.img_link_univ_murcia, "http://catalogo.rebiun.org/rebiun/record/Rebiun07199551"));
        links.add(new Link("La Opinión de Murcia", R.mipmap.img_link_opinion_murcia, "https://www.laopiniondemurcia.es/comunidad/2011/04/17/manuel-campillo-recopila-2000-quehaceres-rurales-libro/316996.html"));
        links.add(new Link("La Verdad", R.mipmap.img_link_la_verdad, "http://www.laverdad.com.es/2011/04/un-jubilado-de-noventa-anos-publica-un.html"));
        links.add(new Link("Presentación y biografía en Murcia.com", R.mipmap.img_link_murcia_com, "https://www.murcia.com/noticias/2011/04/08-diccionario-de-la-vida-campesina.asp"));
        links.add(new Link("Revista La Calle Nº 102, Julio 2011", R.mipmap.img_link_la_calle, "http://www.revistalacalle.com/revista-la-calle-n%C2%BA-101-julio-2011/"));

        return links;
    }
}
