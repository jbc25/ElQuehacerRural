package net.elquehacerrural.app.ui.main;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import net.elquehacerrural.app.R;
import net.elquehacerrural.app.model.Palabra;
import net.elquehacerrural.app.util.Util;

import java.util.List;

public class PalabrasAdapter extends RecyclerView.Adapter<PalabrasAdapter.ViewHolder> {


    private List<Palabra> palabras;
    private Context context;
    private OnItemClickListener itemClickListener;


    public PalabrasAdapter(Context context, List<Palabra> palabras) {
        this.context = context;
        this.palabras = palabras;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View contactView = LayoutInflater.from(context).inflate(R.layout.row_palabra, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position2) {

        Palabra palabra = getItemAtPosition(holder.getAdapterPosition());

        holder.tvPalabraNombre.setText(palabra.getNombre());
        Util.setHtmlLinkableText(holder.tvPalabraDef, palabra.getDefinicion());
//        holder.tvPalabraDef.setText(palabra.getDefinicion());
        holder.btnSeeQuintilla.setVisibility(palabra.hasQuintilla() ? View.VISIBLE : View.GONE);
        holder.btnSeeQuintilla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Palabra palabra = getItemAtPosition(holder.getAdapterPosition());
                new AlertDialog.Builder(context)
                        .setTitle(context.getString(R.string.quintilla_to) + " " + palabra.getNombre())
                        .setMessage(palabra.getQuintilla())
                        .setNegativeButton(R.string.back, null)
                        .setNeutralButton(R.string.share, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                String text = String.format(context.getString(R.string.quintilla_share_text),
                                        palabra.getNombre(), palabra.getQuintilla(),
                                        context.getString(R.string.promo_share_text));

                                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                                sendIntent.putExtra(Intent.EXTRA_TEXT, text);
                                sendIntent.setType("text/plain");
                                context.startActivity(Intent.createChooser(sendIntent,
                                        String.format(context.getString(R.string.share_quintilla), palabra.getNombre())));
                            }
                        })
                        .show();

            }
        });


    }

    @Override
    public int getItemCount() {
        return palabras.size();
    }

    public Palabra getItemAtPosition(int position) {
        return palabras.get(position);
    }

    public void updateData(List<Palabra> palabras) {
        this.palabras = palabras;
        notifyDataSetChanged();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvPalabraDef;
        private final TextView tvPalabraNombre;
        private final Button btnSeeQuintilla;
        public View rootView;

        public ViewHolder(View itemView) {

            super(itemView);

            tvPalabraNombre = (TextView) itemView.findViewById(R.id.tv_palabra_nombre);
            tvPalabraDef = (TextView) itemView.findViewById(R.id.tv_palabra_def);
            btnSeeQuintilla = (Button) itemView.findViewById(R.id.btn_see_quintilla);

            rootView = itemView;
        }

    }


    public void setOnItemClickListener(OnItemClickListener listener) {
        this.itemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}


