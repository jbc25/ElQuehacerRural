package net.elquehacerrural.app.ui.main;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;

import com.google.android.material.navigation.NavigationView;

import net.elquehacerrural.app.App;
import net.elquehacerrural.app.R;
import net.elquehacerrural.app.base.BaseActivity;
import net.elquehacerrural.app.database.PalabraDao;
import net.elquehacerrural.app.model.Palabra;
import net.elquehacerrural.app.ui.InfoActivity;
import net.elquehacerrural.app.ui.links.LinksActivity;
import net.elquehacerrural.app.ui.splash.SplashActivity;
import net.elquehacerrural.app.util.WindowUtils;

import java.util.List;

public class MainActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, TextWatcher, NavigationView.OnNavigationItemSelectedListener {

    private static final int REQ_SPLASH = 11;

    private AppCompatEditText editFilter;
    private AppCompatImageButton btnFilter;
    private AppCompatCheckBox checkOnlyProfessions;
    private RecyclerView recyclerPalabras;
    private String textSearch = "";
    private List<Palabra> palabras;
    private PalabrasAdapter adapter;
    private boolean dontRefreshOnce;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    private void findViews() {
        editFilter = (AppCompatEditText) findViewById(R.id.edit_filter);
        btnFilter = (AppCompatImageButton) findViewById(R.id.btn_filter);
        checkOnlyProfessions = (AppCompatCheckBox) findViewById(R.id.check_only_professions);
        recyclerPalabras = (RecyclerView) findViewById(R.id.recycler_palabras);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);

        navigationView.setNavigationItemSelectedListener(this);
        btnFilter.setOnClickListener(this);
        checkOnlyProfessions.setOnCheckedChangeListener(this);
        editFilter.addTextChangedListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == btnFilter) {

            if (editFilter.getText().toString().isEmpty() && !checkOnlyProfessions.isChecked()) {
                return;
            }

            dontRefreshOnce = true;
            editFilter.setText("");
            dontRefreshOnce = true;
            checkOnlyProfessions.setChecked(false);

            dontRefreshOnce = false;
            refresh();

            WindowUtils.hideSoftKeyboard(this);
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViews();
        configureToolbar();
        configureDrawerLayout();

        if (!getPrefs().getBoolean(App.SHARED_DATA_STORED, false)) {
            startActivityForResult(new Intent(this, SplashActivity.class), REQ_SPLASH);
        }

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerPalabras.setLayoutManager(layoutManager);

        RecyclerView.ItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerPalabras.addItemDecoration(divider);

        palabras = App.getDB().palabraDao().getAll();

        adapter = new PalabrasAdapter(this, palabras);
        recyclerPalabras.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_SPLASH) {
            refresh();
        }
    }

    // CONFIGURATIONS

    private void configureDrawerLayout() {

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
    }

    // INTERACTIONS

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        // Left Navigation
        switch (item.getItemId()) {

            case R.id.menu_presentation:
                InfoActivity.launchInfoActivity(this, InfoActivity.INFO_PRESENTATION);
                break;

            case R.id.menu_biography:
                InfoActivity.launchInfoActivity(this, InfoActivity.INFO_BIOGRAPHY);
                break;

            case R.id.menu_video_poetry:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=Gl1xTqJmLa4")));
                break;

            case R.id.menu_edition_data:
                InfoActivity.launchInfoActivity(this, InfoActivity.INFO_EDITION);
                break;

            case R.id.menu_links:
                startActivity(new Intent(this, LinksActivity.class));
                break;

            case R.id.menu_web:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.elquehacerrural.es")));
                break;

            case R.id.menu_twitter:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/elquehacerrural")));
                break;

            case R.id.menu_share_app:
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_app_text));
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, getString(R.string.share_app_with)));
                break;
        }

        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawer(Gravity.LEFT);
        }

        return false;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        refresh();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        textSearch = charSequence.toString();
        refresh();

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }


    // ----


    private void refresh() {

        if (dontRefreshOnce) {
            dontRefreshOnce = false;
            return;
        }

        List<Palabra> palabrasFilter;
        boolean onlyWithQuintilla = checkOnlyProfessions.isChecked();
        String textSearch = editFilter.getText().toString().trim();

        PalabraDao palabraDao = App.getDB().palabraDao();

//        if (textSearch.isEmpty()) {
//            palabrasFilter = palabraDao.getAll();
//        } else
            if (onlyWithQuintilla) {
            palabrasFilter = palabraDao.searchWithQuintilla(textSearch);
        } else {
            palabrasFilter = palabraDao.search(textSearch);
        }

        palabras.clear();
        palabras.addAll(palabrasFilter);

        adapter.notifyDataSetChanged();
        recyclerPalabras.scrollToPosition(0);
    }


}
