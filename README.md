**Descripción**:  
Diccionario dedicado al mundo campesino en todo el mundo. Su autor es Manuel Campillo Laorden (Santomera, Murcia) y durante 20 años estuvo recopilando términos de distintas fuentes para completarlo con un total de unas 24.000 palabras. A cada oficio del campo le dedica un poema en forma de quintilla.

**Funcionalidades**:

*   Visualización de lista de palabras completa y poema dedicado a los oficios
*   Buscador instantáneo de palabras y filtro para ver solo los oficios con quintilla.
*   Toda la base de datos está guardada en memoria local
*   Secciones de información y enlaces
