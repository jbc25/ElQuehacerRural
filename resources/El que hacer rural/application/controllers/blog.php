<?php
class Blog extends Controller {

	function __construct() {
        parent::Controller();
    }
	
	function index() {
		$data['todo_list'] = array('Clean House', 'Call Mom', 'Run Errands');

		$data['title'] = "My Real Title";
		$data['heading'] = "My Real Heading";
				
		$this->load->view('blogview', $data);
		
		// Para que devuelva datos que procesar (por defecto est� a false)
		$this->load->view('blogview', $data, true);
	}

	function comments()	{
		echo 'Look at this!';
	}
	
	function shoes($sandals, $id) {
        echo $sandals;
        echo $id;
    }
		
	function _remap($method) {
		if ($method == 'shoes') {
			$this->$method('nike', 43);
		} else {
			$this->index();
		}
	}
	
	// funcion oculta (privada) al empezar por '_'
	function _utility() {	
		echo 'nada';
	}
	
	// Para procesar la salida que ir� al navegador
	function _output($output) {
		echo $output;
	}	
}	
?>