<?php // ../controls/editarPalabrasControl.php
require_once '../models/wordCollection.php';
require_once '../view/viewEditarPalabras.php';

Login::start_session();
Login::connect();

ViewEditarPalabras::showHead();

if(	isset($_POST['palabra']) &&
	isset($_POST['definicion']) && 
	isset($_POST['quintilla'])) {
		$palabra = get_post('palabra');
		$definicion = get_post('definicion');
		$quintilla = get_post('quintilla');

		$word = WordCollection::find_word($palabra);
		
		if($word == null) {
			if($quintilla == "") {
				$word = new Word($palabra, $definicion);
			} else {
				$word = new Trade($palabra, $definicion);
				$word->addQuintilla($quintilla);
			}
			$_SESSION['palabras'][] = $word;
		} else {
			// La palabra ya se insert�. Se actualiza!!
			$word->set_name($palabra);
			$word->set_definition($definicion);
			if($word instanceof Trade && $quintilla != "") {
				$word->removeAllQuintillas();
				$word->addQuintilla($quintilla);
			}
		}
		
		if (!$word->save_in_db()) {
			echo "INSERT failed: $query<br />" . mysql_error() . "<br /><br />";	
		}	
		ViewEditarPalabras::showInsertForm();
		ViewEditarPalabras::findWordView("");
} elseif (isset($_POST['palabra']) && isset($_POST['delete'])) {
	$palabra = get_post('palabra');
	if($palabra != "") {		
		$query = "DELETE FROM quintillas WHERE quintilla_palabra_id = (SELECT palabra_id FROM palabras WHERE palabra_nombre='$palabra')";
		if (!mysql_query($query, Login::$db_server))
				echo "DELETE failed: $query<br />" .
				mysql_error() . "<br /><br />";		
		$query = "DELETE FROM palabras WHERE palabra_nombre='$palabra'";
		if (!mysql_query($query, Login::$db_server))
			echo "DELETE failed: $query<br />" .
			mysql_error() . "<br /><br />";
	}
	ViewEditarPalabras::showInsertForm();
	ViewEditarPalabras::findWordView("");
} else if( isset($_POST['palabra']) && isset($_POST['edit'])) {
	$p = ""; $d =""; $q = "";
	$p = get_post('palabra');
	$word = WordCollection::find_word($p);
	if($word != null) {
		$d = $word->get_definition();
		if($word instanceof Trade) {
			if ($word->numQuintillas() > 0) {
				$q = $word->get_quintilla(0);
			}
		}
	}
	ViewEditarPalabras::showEditForm($p, $d, $q);
	ViewEditarPalabras::findWordView("");
} else if( isset($_POST['palabra']) && isset($_POST['find'])) {
	$p = get_post('palabra');
	$words = WordCollection::find_words_like($p, "Off");
	ViewEditarPalabras::showInsertForm();
	ViewEditarPalabras::findWordView($p);
	ViewEditarPalabras::showEditWords($words->get_words());
} else {
	ViewEditarPalabras::showInsertForm();
	ViewEditarPalabras::findWordView("");
}

ViewEditarPalabras::showEnd();

Login::disconnect();

function get_post($var) {
	return mysql_real_escape_string($_POST[$var]);
}
?>