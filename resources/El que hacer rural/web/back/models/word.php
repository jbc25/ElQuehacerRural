<?php
require_once 'login.php';

class Word {	
	protected $id, $name, $definition;
	
	function __construct($n, $def) {
		$this->id = -1;
		$this->name = $n;
		$this->definition = $def;
	}
	
	/*
	function __construct($id_, $n, $def) {
		$this::__construct($n, $def);
		$this->id = $id_;
	}*/	
	
	function get_name() {
		return $this->name;
	}
	
	function set_name($n) {
		$this->name = $n;
	}
	
	function get_definition() {
		return $this->definition;
	}
	
	function set_definition($d) {
		$this->definition = $d;
	}
	
	function set_id($id_) {
		$this->id = $id_;
	}
	
	function get_id() {
		return $this->id;
	}
	
	function save_in_db() {
		if($this->id == -1) {
			// No hay id, palabra nueva. Hay que insertarla.
			$query = "INSERT INTO palabras(palabra_nombre, palabra_definicion) VALUES" .
						"('$this->name', '$this->definition')";
		} else {
			// hay id, hay que actualizar la palabra.
			$query = "UPDATE palabras SET palabra_definicion = '$this->definition' " .
				"WHERE palabra_nombre = '$this->name'";
		}
		$result = mysql_query($query, Login::$db_server);
		if($this->id == -1 && $result) {
			$this->id = mysql_insert_id();
		}
		return $result;
	}
	
	function __destruct() {	
	}
}
?>