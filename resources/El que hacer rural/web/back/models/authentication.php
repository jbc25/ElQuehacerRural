<?php

require_once 'login.php';

if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
    $un_temp = $_SERVER['PHP_AUTH_USER'];
    $pw_temp = $_SERVER['PHP_AUTH_PW'];
    if (Login::start_session($un_temp, $pw_temp)) {
        $_SESSION['username'] = $un_temp;
        $_SESSION['password'] = $pw_temp;
        die("<p><a href=insertaPalabras.php>Pulsa aqu� para continuar</a></p>");
    } else {
        die("Usuario/Contrase�a inv�lida");
        Login::destroy_session_and_data();
    }
} else {
    header('WWW-Authenticate: Basic realm="Restricted Section"');
    header('HTTP/1.0 401 Unauthorized');
    die("Por favor, introduce tu usuario y contrase�a");
}

function mysql_entities_fix_string($string) {
    return htmllentities(mysql_fix_string($string));
}

function mysql_fix_string($string) {
    if (get_magic_quotes_gpc ())
        $string = stripslashes($string);
    return mysql_real_escape__string($string);
}
?>