<?php // buscador.php
require_once '../controls/findWordControl.php';

Login::connect();

$palabrasMax = 50;

echo <<<_END
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	</head>
	<body>
_END;

if(	isset($_POST['palabra'])) {
	$palabra = get_post('palabra');	
	$enOficio = "";
	
	if(isset($_POST['tipoBusqueda'])) {
		$tipoBusqueda = get_post('tipoBusqueda');
	}
	
	if(isset($_POST['enOficio'])) {
		$enOficio = get_post('enOficio');
	}
	
	if($palabra != "" && strlen($palabra) < 3) {
		showMensaje("La palabra a buscar debe tener 3 letras o m�s.");
	} else if($palabra != "") {
		if($tipoBusqueda == "1") {
			$num_word = WordCollection::num_words_like($palabra, $enOficio);
			$words = WordCollection::find_words_like($palabra, $enOficio, 0, $palabrasMax);		
		} else {
			$num_word = WordCollection::num_definitions_contains($palabra, $enOficio);
			$words = WordCollection::find_definitions_contains($palabra, $enOficio, 0, $palabrasMax);
		}	
		$mensajeBusqueda = "<B>$num_word</B> palabras encontradas (Muestra m�ximo $palabrasMax)";
		if($num_word == 1)
			$mensajeBusqueda = "<B>$num_word</B> palabra encontrada (Muestra m�ximo $palabrasMax)";
		
		//$numericos = showNumbers($palabra, $numPalabras, $palabrasMax);
		showMensaje($mensajeBusqueda);
		
		$cadenaPalabras = "";
		for ($i = 0 ; $i < $words->numWords() && $i < $palabrasMax; ++$i) {
			$word = $words->get_word($i);
			$name = $word->get_name();
			$def = "";
			if($tipoBusqueda == "1")
				$def = $word->get_definition();
			else
				$def = str_ireplace("$palabra", "<b><font size=\"5\">$palabra</font></b>", $word->get_definition());
			
			$cadenaPalabras = $cadenaPalabras . 
			"<TR><TD VALIGN=\"TOP\" ALIGN=\"LEFT\"><B>$name:</B></TD>" .
			"<TD VALIGN=\"TOP\" ALIGN=\"JUSTIFY\"> $def </TD></TR>";		
	
			if($word instanceof Trade) {
				for ($j = 0 ; $j < $word->numQuintillas(); ++$j) {
					$quintilla = $word->get_quintilla($j);
					$cadenaPalabras = $cadenaPalabras .
					"<tr><td></td><td VALIGN=\"MIDDLE\" ALIGN=\"CENTER\">
						<TABLE>
						<TR><TD VALIGN=\"MIDDLE\" ALIGN=\"LEFT\"><b><pre>$quintilla</pre></b></TD></TR>
						</TABLE>							
					</td></tr>";
				}
			}	
			$cadenaPalabras = $cadenaPalabras . "<tr><td><br></td></tr>";
		}		
		echo $cadenaPalabras;
		echo <<<_END
			</TABLE>
_END;
	}
} else {
	echo <<<_END
		<TABLE WIDTH="100%" HEIGHT="100%">
		<TR>
		<TD VALIGN="MIDDLE" ALIGN="CENTER">
			<form border="0" action="buscador.php" method="post">
			<table>			
				<tr>					
					<td></td>
					<td><input type="text" name="palabra" /></td>
					<td><input type="submit" value="BUSCAR" /></td>					
				</tr>
				<tr>
					<td>Palabra <input type="Radio" name="tipoBusqueda" value="1" checked="checked"/> |</td>
					<td>En definici�n <input type="Radio" name="tipoBusqueda" value="2"/> ||</td>
					<td>En Oficio <input type="checkbox" name="enOficio"/></td>
			</table>
			</form>
		</TD>
		</TR>
		</TABLE>	
_END;
}

Login::disconnect();

echo <<<_END
</body>
</html>
_END;

function showNumbers($palabra, $numeroTotal, $palabrasMax) {
	for($i = 1; $i <= $numeroTotal/$palabrasMax; $i = ++$i) {
		echo <<<_END
		<form border="0" action="buscador.php" method="post">
		<input type="hidden" name="palabra" value="$palabra"/>
		<input type="hidden" name="indice" value="$i"/>
		<A HREF="buscador.php" TARGET="cuerpo" ALT = "$i" TITLE="$i">$i</A> 
		</form>
_END;
	}
}

function showMensaje($mensaje) {
		echo <<<_END
		<TABLE WIDTH="100%">
			<TR><TD VALIGN="MIDDLE" ALIGN="CENTER">$mensaje</TD></TR>
		</TABLE>
		<TABLE WIDTH="80%" VALIGN="MIDDLE" ALIGN="CENTER">
_END;
}
function get_post($var) {
	return mysql_real_escape_string($_POST[$var]);
}
?>