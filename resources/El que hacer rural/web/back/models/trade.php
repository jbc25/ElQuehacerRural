<?php
require_once 'word.php';

class Trade extends Word {	
	protected $quintillas;
	
	function __construct($n, $def) {
		parent::__construct($n, $def);
		$this->quintillas = array();
	}
	/*
	function __construct($id_, $n, $def) {
		parent::__construct($id_, $n, $def);
		$this->quintillas = array();
	}*/	
	function setId($id) {
		$this->id = $id;
		$this->insert = true;
	}
	
	function removeAllQuintillas() {
		$this->quintillas = array();
	}
	
	function addQuintilla($quintilla) {
		$this->quintillas = array();
		$this->quintillas[] = $quintilla;
	}
	
	function numQuintillas() {
		return count($this->quintillas);
	}
	
	function get_quintilla($index) {
		return $this->quintillas[$index];
	}
	
	function get_quintillas() {
		return $this->quintillas;
	}
	
	function save_in_db() {
		if($this->id == -1) $newWord = true; else $newWord = false;
		parent::save_in_db();
		if($newWord) {
			foreach($this->quintillas as $quinti) {
				$query = "INSERT INTO quintillas(quintilla_palabra_id, quintilla_texto) VALUES" .
					"('$this->id', '$quinti')";
				$result = mysql_query($query, Login::$db_server);
			}
		} else {
			if($this->insert)
				foreach($this->quintillas as $quinti) {
					$query = "INSERT INTO quintillas(quintilla_palabra_id, quintilla_texto) VALUES" .
						"('$this->id', '$quinti')";
					$result = mysql_query($query, Login::$db_server);
				}
			else
				$query = "UPDATE quintillas SET 'quintilla_texto' = '$this->quintilla' " .
				"WHERE 'quintilla_palabra_id' = $this->id";
			$result = mysql_query($query, Login::$db_server);
		}
		return $result;
	}	
	
	function __destruct() {	
	}
}
?>