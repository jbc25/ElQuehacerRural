<?php // insertaPalabras.php
require_once 'wordCollection.php';
Login::start_session();

$j=0;
foreach ($_SESSION['palabras'] as $word) {
	$name = $word->get_name();
	echo "$j: $name<br>";
	++$j;
}

if(!isset($_SESSION['username'])) {
    echo "Por favor pulsa <a href==authenticate.php>aqui</a> para identificarse de nuevo";
    return;
}

Login::connect();

if(	isset($_POST['palabra']) &&
	isset($_POST['definicion']) && 
	isset($_POST['quintilla'])) {
		$palabra = get_post('palabra');
		$definicion = get_post('definicion');
		$quintilla = get_post('quintilla');

		$word = WordCollection::find_word($palabra);
		
		if($word == null) {
			if($quintilla == "") {
				$word = new Word($palabra, $definicion);
			} else {
				$word = new Trade($palabra, $definicion);
				$word->addQuintilla($quintilla);
			}
			$_SESSION['palabras'][] = $word;
		} else {
			// La palabra ya se insert�. Se actualiza!!
			$word->set_definition($definicion);
			if($word instanceof Trade && $quintilla != "") {
				$word->addQuintilla($quintilla);
			}
		}
		
		if (!$word->save_in_db()) {
			echo "INSERT failed: $query<br />" . mysql_error() . "<br /><br />";	
		}			
} elseif (	isset($_POST['palabra']) &&
			isset($_POST['delete'])) {
	$palabra = get_post('palabra');
	if($palabra != "") {		
		$query = "DELETE FROM quintillas WHERE quintilla_palabra_id = (SELECT palabra_id FROM palabras WHERE palabra_nombre='$palabra')";
		if (!mysql_query($query, Login::$db_server))
				echo "DELETE failed: $query<br />" .
				mysql_error() . "<br /><br />";		
		$query = "DELETE FROM palabras WHERE palabra_nombre='$palabra'";
		if (!mysql_query($query, Login::$db_server))
			echo "DELETE failed: $query<br />" .
			mysql_error() . "<br /><br />";
	}
}

$p = "";
$d = "";
$q = "";

if( isset($_POST['palabra']) && isset($_POST['edit'])) {
	$p = get_post('palabra');
	$word = WordCollection::find_word($p);
	if($word != null) {
		$d = $word->get_definition();
		if($word instanceof Trade) {
			if ($word->numQuintillas() > 0) {
				$q = $word->get_quintilla(0);
			}
		}
	}
}

echo <<<_END
<form border="0" action="insertaPalabras.php" method="post">
<table>
<tr>
<td>Palabra</td>
<td><input type="text" name="palabra" value="$p"/></td>
</tr>
<tr>
<td>Definicion</td>
<td><textarea name="definicion" cols="100" rows="10" wrap="soft">$d</textarea></td>
</tr>
<tr>
<td>Quintilla</td>
<td><textarea name="quintilla" cols="50" rows="5" wrap="off">$q</textarea></td>
</tr>
</table>
<input type="submit" value="A�ADIR PALABRA" />
</form>
_END;

$query = "SELECT * FROM palabras";
$resultPalabras = mysql_query($query);
if (!$resultPalabras) die ("Database access failed: " . mysql_error());
$rowsPalabras = mysql_num_rows($resultPalabras);
reset($_SESSION['palabras']);
foreach ($_SESSION['palabras'] as $word) {
	$pal = $word->get_name();
	$def = $word->get_definition();
	echo <<<_END
	<table>
		<tr>
			<td><form action="insertaPalabras.php" method="post">
				<input type="hidden" name="delete" value="yes" />
				<input type="hidden" name="palabra" value="$pal" />
				<input type="submit" value="BORRAR" /></form>
				<form action="insertaPalabras.php" method="post">
				<input type="hidden" name="edit" value="yes" />
				<input type="hidden" name="palabra" value="$pal" />
				<input type="submit" value="EDITAR" /></form>
			</td>
			<td valign="LEFT" align="TOP"><b>$pal:</b></td>
			<td>$def</td>					
		</tr>
_END;
	if($word instanceof Trade) {
		$quintillas = $word->get_quintillas();
		foreach ($quintillas as $quinti) {
			echo <<<_END
				<tr>
					<td></td>
					<td>Quintilla:</td>
					<td><b><pre>$quinti</pre></b></td>
				</tr>
_END;
		}
	}
echo <<<_END
	</table>
_END;
}

Login::disconnect();

function get_post($var) {
	return mysql_real_escape_string($_POST[$var]);
}
?>