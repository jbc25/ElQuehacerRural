<?php
require_once 'trade.php';

class WordCollection {	
	protected $words;
	
	function __construct() {
		$this->words = array();
	}	
	
	function addWord($w) {
		$this->words[] = $w;
	}
	
	function numWords() {
		return count($this->words);
	}
	
	function get_words() {
		return $this->words;
	}
	function get_word($index) {
		return $this->words[$index];
	}
	
	static function num_words_like($word, $enOficio) {
		if($enOficio == "on") {
			$query = "SELECT COUNT(*) FROM palabras,quintillas WHERE palabra_id=quintilla_palabra_id AND palabra_nombre LIKE '$word%'";
		} else {
			$query = "SELECT COUNT(*) FROM palabras WHERE palabra_nombre LIKE '$word%'";
		}
		$result = mysql_query($query);		
		$result = mysql_fetch_array($result); 
        return $result[0]; 
	}
	
	static function num_definitions_contains($word, $enOficio) {
		if($enOficio == "on") {
			$subquery = "SELECT COUNT(*) FROM palabras,quintillas WHERE palabra_id=quintilla_palabra_id AND palabra_definicion LIKE '% $word %'";
		} else {
			$subquery = "SELECT COUNT(*) FROM palabras WHERE palabra_definicion LIKE '% $word %'";
		}		
		$result = mysql_query($subquery);		
		$result = mysql_fetch_array($result); 
        return $result[0];
	}
	
	static function find_word($word) {
		$subquery = "palabras WHERE palabra_nombre = '$word'";
		$wc = self::load_from_db($subquery);
		if($wc->numWords() == 1) {
			return $wc->get_word(0);
		} else
			return null;
	}
	
	static function find_words_like($word, $enOficio, $inicio, $fin) {
		if($enOficio == "on") {
			$query = "palabras,quintillas WHERE palabra_id=quintilla_palabra_id AND palabra_nombre LIKE '$word%' ORDER BY palabra_nombre LIMIT $inicio,$fin";
			return self::load_from_db($query);
		} else {
			$query = "palabras WHERE palabra_nombre LIKE '$word%' ORDER BY palabra_nombre LIMIT $inicio,$fin";
			return self::load_from_db($query);
		}		
	}
	
	static function find_definitions_contains($word, $enOficio, $inicio, $fin) {
		if($enOficio == "on") {
			$subquery = "palabras,quintillas WHERE palabra_id=quintilla_palabra_id AND palabra_definicion LIKE '% $word %' ORDER BY palabra_nombre LIMIT $inicio,$fin";
		} else {
			$subquery = "palabras WHERE palabra_definicion LIKE '% $word %' ORDER BY palabra_nombre LIMIT $inicio,$fin";
		}		
		return self::load_from_db($subquery);	
	}
	
	static function load_from_db($subquery) {
		$query = "SELECT palabra_id, palabra_nombre, palabra_definicion FROM " . $subquery;
		$collection = new WordCollection;
				
		$resultPalabras = mysql_query($query);
		$rowsPalabras = mysql_num_rows($resultPalabras);
		
		$cadenaPalabras = "";
		for ($i = 0 ; $i < $rowsPalabras /*&& $i < 10*/ ; ++$i) {
			$rowPalabras = mysql_fetch_row($resultPalabras);	

			$queryQuin = "SELECT quintilla_texto FROM quintillas, palabras WHERE quintilla_palabra_id=palabra_id && palabra_nombre='$rowPalabras[1]'";
			$resultQuintillas = mysql_query($queryQuin);
			if (!$resultQuintillas) die ("Database access failed: " . mysql_error());
			$rowsQuintillas = mysql_num_rows($resultQuintillas);
			$newWord = new Word($rowPalabras[1], $rowPalabras[2]);
			$newWord->set_id($rowPalabras[0]);
			if($rowsQuintillas > 0) {
				$newWord = new Trade($rowPalabras[1], $rowPalabras[2]);
				$newWord->set_id($rowPalabras[0]);
				for ($j = 0 ; $j < $rowsQuintillas ; ++$j) {
					$rowQuintilla = mysql_fetch_row($resultQuintillas);
					$newWord->addQuintilla($rowQuintilla[0]);
				}
			}		
			$collection->addWord($newWord);
		}		
		return $collection;
	}	
	
	function __destruct() {	
	}
}
?>