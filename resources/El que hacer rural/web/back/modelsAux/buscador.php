<?php // buscador.php
require_once '../controls/findWordControl.php';

Login::connect();

$palabrasMax = 50;

echo <<<_END
<html>
	<head>
	</head>
	<body>
_END;

if(	isset($_POST['palabra'])) {
	$palabra = get_post('palabra');	
	$enOficio = "";
	
	echo <<<_END
		<TABLE WIDTH="100%">
		<TR>
		<TD VALIGN="MIDDLE" ALIGN="CENTER">
			<table>
				<form border="0" action="buscador.php" method="post">
				<tr>
					<input type="text" name="palabra" value="$palabra"/>
					<input type="submit" value="BUSCAR" />									
				</tr>
				<tr>
_END;
	if(!isset($_POST['tipoBusqueda'])) {	
		echo <<<_END
					<td>Palabra <input type="Radio" name="tipoBusqueda" value="1" checked="checked"/> |</td>
					<td>En definici�n <input type="Radio" name="tipoBusqueda" value="2"/> ||</td>
_END;
	} else {
		$tipoBusqueda = get_post('tipoBusqueda');	
		if($tipoBusqueda == "1") {
			echo <<<_END
					<td>Palabra <input type="Radio" name="tipoBusqueda" value="1" checked="checked"/> |</td>
					<td>En definici�n <input type="Radio" name="tipoBusqueda" value="2"/> ||</td>
_END;
		} else {
			echo <<<_END
					<td>Palabra <input type="Radio" name="tipoBusqueda" value="1" /> |</td>
					<td>En definici�n <input type="Radio" name="tipoBusqueda" value="2" checked="checked"/> ||</td>
_END;
		}
	}
	
	if(isset($_POST['enOficio'])) {
		$enOficio = get_post('enOficio');
		if($enOficio == "on") {
			echo <<<_END
					<td>En Oficio <input type="checkbox" name="enOficio" checked="checked"/></td>
_END;
		}
	} else {
		echo <<<_END
				<td>En Oficio <input type="checkbox" name="enOficio"/></td>
				</tr>
				</form>
			</table>
		</TD>
		</TR>
		</TABLE>	
_END;
	}
	if($palabra != "") {
		if($tipoBusqueda == "1") {
			$words = WordCollection::find_words_like($palabra, $enOficio);
		} else {
			$words = WordCollection::find_definitions_contains($palabra, $enOficio);
		}
		$num_word = $words->numWords();		
		$mensajeBusqueda = "<B>$num_word</B> palabras encontradas (Muestra m�ximo $palabrasMax)";
		if($num_word == 1)
			$mensajeBusqueda = "<B>$num_word</B> palabra encontrada (Muestra m�ximo $palabrasMax)";
			
		echo <<<_END
		<TABLE WIDTH="100%">
			<TR><TD VALIGN="MIDDLE" ALIGN="CENTER">$mensajeBusqueda</TD></TR>
		</TABLE>
		
		<TABLE WIDTH="80%" VALIGN="MIDDLE" ALIGN="CENTER">
_END;
		
		$cadenaPalabras = "";
		for ($i = 0 ; $i < $words->numWords() && $i < $palabrasMax; ++$i) {
			$word = $words->get_word($i);
			$name = $word->get_name();
			$def = "";
			if($tipoBusqueda == "1")
				$def = $word->get_definition();
			else
				$def = str_replace("$palabra", "<b><font size=\"5\">$palabra</font></b>", $word->get_definition());
			
			$cadenaPalabras = $cadenaPalabras . 
			"<TR><TD VALIGN=\"TOP\" ALIGN=\"LEFT\"><B>$name:</B></TD>" .
			"<TD VALIGN=\"TOP\" ALIGN=\"JUSTIFY\"> $def </TD></TR>";		
	
			if($word instanceof Trade) {
				for ($j = 0 ; $j < $word->numQuintillas(); ++$j) {
					$quintilla = $word->get_quintilla($j);
					$cadenaPalabras = $cadenaPalabras .
					"<tr><td></td><td VALIGN=\"MIDDLE\" ALIGN=\"CENTER\"><b><pre>$quintilla</pre></b></td></tr>";
				}
			}	
		}		
		echo $cadenaPalabras;
		echo <<<_END
			</TABLE>
_END;
	}
} else {
	echo <<<_END
		<TABLE WIDTH="100%" HEIGHT="100%">
		<TR>
		<TD VALIGN="MIDDLE" ALIGN="CENTER">
			<form border="0" action="buscador.php" method="post">
			<table>			
				<tr>					
					<td></td>
					<td><input type="text" name="palabra" /></td>
					<td><input type="submit" value="BUSCAR" /></td>					
				</tr>
				<tr>
					<td>Palabra <input type="Radio" name="tipoBusqueda" value="1" checked="checked"/> |</td>
					<td>En definici�n <input type="Radio" name="tipoBusqueda" value="2"/> ||</td>
					<td>En Oficio <input type="checkbox" name="enOficio"/></td>
			</table>
			</form>
		</TD>
		</TR>
		</TABLE>	
_END;
}

Login::disconnect();

echo <<<_END
</body>
</html>
_END;
function get_post($var) {
	return mysql_real_escape_string($_POST[$var]);
}
?>