<?php // ../view/viewEditarPalabras.php
require_once '../models/wordCollection.php';

class ViewEditarPalabras {
    
	static function showHead() {
            echo <<<_END
	<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	</head>
	<body>
_END;
	}

	static function showEnd() {
            echo <<<_END
	</body>
	</html>
_END;
	}

	static function showEditForm($palabra, $definicion, $quintilla) {
		echo <<<_END
<form border="0" action="../controls/editarPalabrasControl.php" method="post">
<table>
<tr>
<td>Palabra</td>
<td><input type="text" name="palabra" value="$palabra"/></td>
</tr>
<tr>
<td>Definicion</td>
<td><textarea name="definicion" cols="100" rows="10" wrap="soft">$definicion</textarea></td>
</tr>
<tr>
<td>Quintilla</td>
<td><textarea name="quintilla" cols="50" rows="5" wrap="off">$quintilla</textarea></td>
</tr>
</table>
<input type="submit" value="MODIFICAR PALABRA" />
</form>
_END;
	}
	
	static function showInsertForm() {
		echo <<<_END
<form border="0" action="../controls/editarPalabrasControl.php" method="post">
<table>
<tr>
<td>Palabra</td>
<td><input type="text" name="palabra"/></td>
</tr>
<tr>
<td>Definicion</td>
<td><textarea name="definicion" cols="100" rows="10" wrap="soft"></textarea></td>
</tr>
<tr>
<td>Quintilla</td>
<td><textarea name="quintilla" cols="50" rows="5" wrap="off"></textarea></td>
</tr>
</table>
<input type="submit" value="A�ADIR PALABRA" />
</form>
_END;
	}
	
	static function showEditWords($words) {
		foreach ($words as $word) {
			$pal = $word->get_name();
			$def = $word->get_definition();
	echo <<<_END
	<table>
		<tr>
			<td><form action="../controls/editarPalabrasControl.php" method="post">
				<input type="hidden" name="delete" value="yes" />
				<input type="hidden" name="palabra" value="$pal" />
				<input type="submit" value="BORRAR" /></form>
				<form action="../controls/editarPalabrasControl.php" method="post">
				<input type="hidden" name="edit" value="yes" />
				<input type="hidden" name="palabra" value="$pal" />
				<input type="submit" value="EDITAR" /></form>
			</td>
			<td valign="LEFT" align="TOP"><b>$pal:</b></td>
			<td>$def</td>					
		</tr>
_END;
			if($word instanceof Trade) {
				$quintillas = $word->get_quintillas();
				foreach ($quintillas as $quinti) {
					echo <<<_END
						<tr>
							<td></td>
							<td>Quintilla:</td>
							<td><b><pre>$quinti</pre></b></td>
						</tr>	
_END;
				}
			}
		echo <<<_END
			</table>
_END;
		}
	}
	
	static function findWordView($word) {
		echo <<<_END
		<TABLE WIDTH="100%">
		<TR>
		<TD VALIGN="MIDDLE" ALIGN="CENTER">
			<table>
				<form border="0" action="../controls/editarPalabrasControl.php" method="post">
				<tr>
					<input type="hidden" name="find" value="yes" />
					<input type="text" name="palabra" value="$word"/>
					<input type="submit" value="BUSCAR" />									
				</tr>
				</form>
			</table>
		</TD></TR>
		</TABLE>
_END;
	}
}
?>